﻿<%@ Page Title="Contact Us - Eco Green Technologies | Quality Water Mangement Plants, Solar Products sales in Coimbatore, water treatment plant in chennai, water treatment plant design in coimbatore, 
water treatment plant manufacturers in coimbatore,
water treatment plant suppliers in coimbatore,
sewage treatment plants in coimbatore,
ro water treatment plant manufacturers in coimbatore,
ro water in coimbatore,
ro water plant in coimbatore,
ro system in coimbatore,
water softener manufacturers in coimbatore,
domestic water softener in coimbatore,
domestic water treatment manufacturers in coimbatore,
industrial ro plant manufacturer in coimbatore,
industrial water treatment suppliers in coimbatore,
effluent water treatment plant manufacturers in coimbatore,
mineral water plant manufacturers in coimbatore,
ultrafiltration water treatment plant manufacturers in coimbatore,
solar power plant in coimbatore,
solar panels manufacturesin coimbatore,
water softener dealers in coimbatore,
sewage treatment plant design in coimbatore,
water treatment construction in coimbatore,
water treatment plants construction dealers in coimbatore,
wastewater treatment plant manufacturers in coimbatore,
wastewater treatment plant in coimbatore,
wastewater treatment plant suppliers in coimbatore,
solar water heating system manufacturers in coimbatore,
solar water heater suppliers in coimbatore,
solar water heater in coimbatore,
solar products manufacturers in coimbatore,
environmental consultant in coimbatore" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EcoGreenWebsite.Contact1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- mail -->
	<div class="w3_map">
    <%--<iframe src="" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15663.582877761557!2d77.0518237!3d11.0464447!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2e1a394f57240ff6!2sEco+Green+Tech!5e0!3m2!1sen!2sin!4v1539340839991" allowfullscreen></iframe></div>
	<div class="mail">
		<div class="container">
			<h2 class="w3l_head w3l_head1">Contact Us</h2>
			<div class="agile_mail_grids">
				<div class="agile_mail_grid">
					<form action="#" runat="server" method="post">
						<div class="col-md-6 agile_mail_grid_left">
							<input type="text" name="Name" id="txtName" runat="server" placeholder="Name" required="">
							<input type="text" name="Phone" id="txtMobile" runat="server" placeholder="Phone" required="">
						</div>
						<div class="col-md-6 agile_mail_grid_left">
							<input type="text" name="Email" id="txtEmail" runat="server" placeholder="Email" required="">
							<!-- <input type="text" name="Subject" placeholder="Subject" required=""> -->
						</div>
						<div class="clearfix"> </div>
						<textarea name="Message" id="txtMsg" runat="server" placeholder="Message..." required=""></textarea>
                        <asp:Button ID="submit" Text="Send Message" runat="server" OnClick="btnSubmit_Click" />
						<!-- <input type="submit" value="Submit Now"> -->
					</form>
				</div>
			</div>
			<div class="agile_mail_grid1">
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						<h4>Contact By Email</h4>
						<p>We are happy to reply for your clarifications and feedbacks through emails. Please feel free to mail Us</p>
						<ul>
							<li><strong><a href="mailto:info@example.com">ecogreentamilnadu@gmail.com</a></strong></li>
                            <li><strong><a href="mailto:info@example.com">director@ecogreentech.co.in</a></strong></li>
							
						</ul>
					</div>
				</div>
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
						<h4>Contact By Phone</h4>
						<p>We are glad to hear from you 24/7. Please feel free to Contact Us</p>
						<ul>
							<li><strong> +91 99529 99799 </strong></li>
							<li><strong> +91 70949 99799 </strong></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						<h4>Looking For Address</h4>
						<p>Our team members are  happy to welcome you to our Place.You are most welcome in our working hours.</p>
                        <h4  >Head Office</h4>
						<ul>
							<li> No: 43J, Ramraj Nagar,</li>
							<li> Goldwins,</li>
                            <li>Coimbatore – 641 062</li>
                            <li>Ph: +91-99529 99799</li>
						</ul>
                        <h4>Branch</h4>
                        <ul>
							<li>Branch Office: N0.9(1), Keerthana Complex, </li>
							<li>Binny Compound,Kumaran Road,</li>
                            <li>Tirupur-641 601</li>
                            <li>Ph: +91-90959 99099</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //mail -->
</asp:Content>
