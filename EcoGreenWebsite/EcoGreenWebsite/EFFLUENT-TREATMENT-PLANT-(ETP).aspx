﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Wastewater5.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>EFFLUENT TREATMENT PLANT</h4>
										<p>Effluent treatment plant cleans industrial effluents, contaminated water from rivers and lakes, and so on just in order to reuse the water for additional purposes. Along such lines, water is reutilized and sustained. In fact, such gushing treatment ensures that any contaminant will be expelled from the water making it reusable. It is mostly used in industries such as pharmaceuticals, textiles, tanneries, and chemicals where there is a chance of extreme water contamination. Nevertheless, how this treatment will be applied may vary from industry to industry.
</p>
										<p>Industrial wastewater treatment describes the processes used for treating wastewater that is produced by industries as an undesirable by-product. After treatment, the treated industrial wastewater (or effluent) may be reused or released to a sanitary sewer or to a surface water in the environment.
</p>
										
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
