﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Wastewater2.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>MBBR BASED SEWAGE TREAMENT PLANT</h4>
										<p>The MBBR system consists of an aeration tank (similar to an activated sludge tank) with special plastic carriers that provide a surface where a biofilm can grow. The carriers are made of a material with a density close to the density of water. The carriers will be mixed in the tank by the aeration system and thus will have good contact between the substrate in the influent wastewater and the biomass on the carriers. To prevent the plastic carriers from escaping the aeration it is necessary to have a sieve on the outlet of the tank. To achieve higher concentration of biomass in the bioreactors, hybrid MBBR systems have also been used where suspended and attached biomass co-exist contributing both to biological processes. 
</p>
										<p>Some other advantages compared to activated sludge systems are
</p>
										<ul>
											<li>Higher effective sludge retention time (SRT) which is favorable for nitrification</li>
                                            <li>Responds to load fluctuations without operator intervention</li>
                                            <li>Lower sludge production</li>
                                            <li>Less area required</li>
                                            <li>Resilient to toxic shock</li>
											<li>Process performance independent of secondary clarifier (due to the fact that there is no sludge return line)</li>
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
