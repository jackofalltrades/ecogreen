﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Wastewater4.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>MBR BASED SEWAGE TREAMENT PLANT</h4>
										<p>Membrane bioreactor (MBR) is the combination of a membrane process like microfiltration or ultrafiltration with a biological wastewater treatment process, the activated sludge process. It is now widely used for municipal and industrial wastewater treatment.
</p>
										<p>Advantages of MBR Systems:
</p>
										<ul>
											<li>Less Area required.</li>
                                            <li>Less Power Consumption.</li>
                                            <li>PSF & ACF(Filters) are not required.</li>
                                            <li>Quality of treated water is high when compered to other method.</li>
                                            <li>High removal efficiency of BOD,COD and Nutrients. </li>
											<li>Lesser Sludge Generation.</li>
											<li>Fully Automatic System with PLC System.</li>
											<li>Plug and Play System (Easy Installation and Commissioning)</li>
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
