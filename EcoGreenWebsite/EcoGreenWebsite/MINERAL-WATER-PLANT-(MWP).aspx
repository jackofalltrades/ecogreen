﻿<%@ Page Title="MINERAL WATER PLANT (MWP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="MINERAL-WATER-PLANT-(MWP).aspx.cs" Inherits="EcoGreenWebsite.MINERAL_WATER_PLANT__MWP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/6.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>MINERAL WATER PLANT (MWP)</h4>
										<p>Mineral Water / Packaged Drinking Water Project consist of following
Raw Water Source	PET BOTTLE blowing Machine
Raw Water Transfer	PET BOTTLE Rinse, Fill, Cap, Label Machine
Raw Water Storage	POUCH Form, Fill & Seal machine
Water Treatment System	GLASS Rinse, Fill & Seal machine
Water Testing Facility	20 Ltr JAR Washing & Filling Machine
Production Machineries	Injection Molding Machine
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
