﻿<%@ Page Title="WATER SOFTNER | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="WATER-SOFTNER.aspx.cs" Inherits="EcoGreenWebsite.WATER_SOFTNER1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--content-->
    <div class="content">

        <div class="advantages-w3l">
            <div class="container">
                <h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
                <div class="advantages-grids">
                    <div class="col-md-6 advantages-grid">
                        <div class="ser-img">
                            <img class="img-responsive" src="images/Product/Solar3.jpg" alt=" " />
                        </div>
                    </div>
                    <div class="col-md-6 advantages-grid">
                        <div class="care">
                            <div class="left-grid">
                                <p>#</p>
                            </div>
                            <div class="right-grid">
                                <h4>OPERATIONS AND MAINTENANCE</h4>
                                <p>
                                    The Effluent Treatment plant or Sewage Treatment Plant once constructed/established need to have operation & maintenance through qualified & experience persons to serve efficient & effective operation of ETP/STP, qualified operator & supervisor are being provided by Perfect Pollution Services who maintains all the records of the operation and attends the day to day trouble shooting.
                                </p>
                                <p>
                                    •Various unit operations and processes (Separation by Screening, sedimentation , filtration, Neutralisation, coagulation, Flocculation, Absorption, Adsorption, Chemical Reaction, Oxidation/Reduction, Dissolution, Ion exchange, Chlorination)


                                </p>
                                <p>
                                    •Operation of Units such as Screens (Coarse / Fine Bar screens, Manual/ Mechanically operated), Sumps and Pumping Stations including Pumps, Motors and Panels (Centrifugal Horizontal / Vertical Turbine), Valves(Sluice gates, Non return, Reflux), Pipes/Specials and Pipe Joints, Grit Removal Units. Primary Sedimentation / Settling tanks, Scraping Mechanisms, Sludge withdrawal, Sludge Sumps, Sludge Pumps, Aeration tanks and Aerators, Secondary Settling Tanks, Secondary Sludge Sumps, Pumps, Sludge Thickeners, Sludge Digesters, Gas Production, Sludge handling and Drying.


                                </p>
                                <p>
                                    •Maintenance of Pumps and Motors, Electrical Panels (Starters, Meter, (Energy, Voltage, Amperage, Power factor), Blow out Fuses, Valves, Gates, Scraping Bridge Trolley, Aerators, Reduction Gears, Open Air Weather Casings for Motors, Sprocket wheels and Chains for Mechanical Grit and Screen removing devices.


                                </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--content-->
</asp:Content>


