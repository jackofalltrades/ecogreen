﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar5.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>REVAMPING AND RECOMMISSIONING OF EXISTING </h4>
										<p>We firmly believe in optimum utilization of existing plants. We offer comprehensive revamping and Recommissioning options to our customers. We study existing systems of clients and offer detailed options for meeting standards, capacity enhancement and optimum utilization of existing electro mechanical equipment.
</p>
										<p>We offer following services for Revamping and Recommissioning of Plants
</p>
										<ul>
											<li>Detailed on site study of existing systems by our qualified engineers</li>
                                            <li>In depth analysis for under performance of systems</li>
                                            <li>On Site study and trails of capacity enhancements</li>
                                            <li>Detailed reports, presentations on operational failures</li>
                                            <li>Detailed reports on proper size of equipment needed to run the plants</li>
											<li>Calibration of all electro mechanical equipments</li>
											<li>Validation for investments to revamps or Recommission the plants</li>
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
