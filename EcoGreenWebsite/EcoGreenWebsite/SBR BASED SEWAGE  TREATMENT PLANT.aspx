﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Wastewater3.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SBR SEWAGE TREAMENT PLANT</h4>
										<p>Sequencing batch reactors (SBR) or sequential batch reactors are a type of activated sludge process for the treatment of wastewater. SBR reactors treat wastewater such as sewage or output from anaerobic digesters or mechanical biological treatment facilities in batches. Oxygen is bubbled through the mixture of wastewater and activated sludge to reduce the organic matter (measured as biochemical oxygen demand (BOD) and chemical oxygen demand (COD)). The treated effluent may be suitable for discharge to surface waters or possibly for use on land.
</p>
										<p>While there are several configurations of SBRs, the basic process is similar. The installation consists of one or more tanks that can be operated as plug flow or completely mixed reactors. The tanks have a “flow through” system, with raw wastewater (influent) coming in at one end and treated water (effluent) flowing out the other. In systems with multiple tanks, while one tank is in settle/decant mode the other is aerating and filling. In some systems, tanks contain a section known as the bio-selector, which consists of a series of walls or baffles which direct the flow either from side to side of the tank or under and over consecutive baffles. This helps to mix the incoming Influent and the returned activated sludge (RAS), beginning the biological digestion process before the liquor enters the main part of the tank
</p>
										
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
