﻿<%@ Page Title="SOLAR POWER PLANT | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SOLAR-POWER-PLANT.aspx.cs" Inherits="EcoGreenWebsite.SOLAR_POWER_PLANT1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar2.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR POWER PLANT</h4>
										<p>Solar power is pollution free and causes no greenhouse gases to be emitted after installation.
</p>
										<p>Reduced dependence on foreign oil and fossil fuels.Renewable clean power that is available every day of the year, even cloudy days  produce some power.


</p>
										<p>Return on investment unlike paying for utility bills.Virtually no maintenance as solar panels last over 30 years.

</p>
										<p>Creates jobs by employing solar panel manufacturers, solar installers, etc. and in turn helps the economy.
</p>
										<p>Excess power can be sold back to the power company if grid intertied.
</p>
										<p>Ability to live grid free if all power generated provides enough for the home / building..
</p>
										<p>Can be installed virtually anywhere; in a field to on a building.
</p>
										<p>Use batteries to store extra power for use at night.Solar can be used to heat water, power homes and building, even power cars.

</p>
										<p>Safer than traditional electric current.Efficiency is always improving so the same size solar that is available today will become more efficient tomorrow.

</p>
										<p>Aesthetics are improving making the solar more versatile compared to older models; i.e. printing, flexible, solar shingles, etc.
</p>
										<p>Federal grants, tax incentives, and rebate programs are available to help with initial costs.
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
