﻿<%@ Page Title="SOLAR STREET LIGHT | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SOLAR-STREET-LIGHT.aspx.cs" Inherits="EcoGreenWebsite.SOLAR_STREET_LIGHT1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/10.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR STREET LIGHT</h4>
										<p>We are among the renowned manufacturers and suppliers of wide range of Solar Street Lights that are available with different power ranges and consist of sodium vapors, LED's and CFL's. Our lights can easily operate in misty and foggy conditions. These products are resistant to adverse weather conditions and requires minimum maintenance. Our entire product range can be customized as per the clients as per the clients’ requirement and preference.</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
