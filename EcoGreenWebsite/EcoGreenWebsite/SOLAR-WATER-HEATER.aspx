﻿<%@ Page Title="SOLAR WATER HEATER | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SOLAR-WATER-HEATER.aspx.cs" Inherits="EcoGreenWebsite.SOLAR_WATER_HEATER1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar1.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR WATER HEATER</h4>
										<p>We vast array of ECO GREEN Solar Water Heater ETC is specially designed to address the requirements of domestic applications. Reputed for their features of environment friendliness, energy efficiency. The range offers great convenience to users. We offer these in diverse capacities, to suit the requirements of our customers.

</p>
										<ul>
											<li>EVACUVATED TUBE COLLECTOR (ETC) MODEL</li>
                                            <li>FLAT PLATE COLLECTOR (FPC) MODEL</li>
                                            
											
										</ul>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
