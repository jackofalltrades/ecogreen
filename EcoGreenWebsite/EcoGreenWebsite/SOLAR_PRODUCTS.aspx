﻿<%@ Page Title="INDUSTRIAL R.O PLANT  - Eco Green Technologies | Quality Water Mangement Plants, Solar Products sales in Coimbatore, water treatment plant in chennai, water treatment plant design in coimbatore, 
water treatment plant manufacturers in coimbatore,
water treatment plant suppliers in coimbatore,
sewage treatment plants in coimbatore,
ro water treatment plant manufacturers in coimbatore,
ro water in coimbatore,
ro water plant in coimbatore,
ro system in coimbatore,
water softener manufacturers in coimbatore,
domestic water softener in coimbatore,
domestic water treatment manufacturers in coimbatore,
industrial ro plant manufacturer in coimbatore,
industrial water treatment suppliers in coimbatore,
effluent water treatment plant manufacturers in coimbatore,
mineral water plant manufacturers in coimbatore,
ultrafiltration water treatment plant manufacturers in coimbatore,
solar power plant in coimbatore,
solar panels manufacturesin coimbatore,
water softener dealers in coimbatore,
sewage treatment plant design in coimbatore,
water treatment construction in coimbatore,
water treatment plants construction dealers in coimbatore,
wastewater treatment plant manufacturers in coimbatore,
wastewater treatment plant in coimbatore,
wastewater treatment plant suppliers in coimbatore,
solar water heating system manufacturers in coimbatore,
solar water heater suppliers in coimbatore,
solar water heater in coimbatore,
solar products manufacturers in coimbatore,
environmental consultant in coimbatore" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="INDUSTRIAL-RO-PLANT.aspx.cs" Inherits="EcoGreenWebsite.INDUSTRIAL_RO_PLANT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar1.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR WATER HEATER</h4>
										<p>We vast array of ECO GREEN Solar Water Heater ETC is specially designed to address the requirements of domestic applications. Reputed for their features of environment friendliness, energy efficiency. The range offers great convenience to users. We offer these in diverse capacities, to suit the requirements of our customers.

</p>
										<ul>
											<li>EVACUVATED TUBE COLLECTOR (ETC) MODEL</li>
                                            <li>FLAT PLATE COLLECTOR (FPC) MODEL</li>
                                            
											
										</ul>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar2.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR POWER PLANT</h4>
										<p>Solar power is pollution free and causes no greenhouse gases to be emitted after installation.
</p>
										<p>Reduced dependence on foreign oil and fossil fuels.Renewable clean power that is available every day of the year, even cloudy days  produce some power.


</p>
										<p>Return on investment unlike paying for utility bills.Virtually no maintenance as solar panels last over 30 years.

</p>
										<p>Creates jobs by employing solar panel manufacturers, solar installers, etc. and in turn helps the economy.
</p>
										<p>Excess power can be sold back to the power company if grid intertied.
</p>
										<p>Ability to live grid free if all power generated provides enough for the home / building..
</p>
										<p>Can be installed virtually anywhere; in a field to on a building.
</p>
										<p>Use batteries to store extra power for use at night.Solar can be used to heat water, power homes and building, even power cars.

</p>
										<p>Safer than traditional electric current.Efficiency is always improving so the same size solar that is available today will become more efficient tomorrow.

</p>
										<p>Aesthetics are improving making the solar more versatile compared to older models; i.e. printing, flexible, solar shingles, etc.
</p>
										<p>Federal grants, tax incentives, and rebate programs are available to help with initial costs.
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar3.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>OPERATIONS AND MAINTENANCE</h4>
										<p>The Effluent Treatment plant or Sewage Treatment Plant once constructed/established need to have operation & maintenance through qualified & experience persons to serve efficient & effective operation of ETP/STP, qualified operator & supervisor are being provided by Perfect Pollution Services who maintains all the records of the operation and attends the day to day trouble shooting.
</p>
										<p>•Various unit operations and processes (Separation by Screening, sedimentation , filtration, Neutralisation, coagulation, Flocculation, Absorption, Adsorption, Chemical Reaction, Oxidation/Reduction, Dissolution, Ion exchange, Chlorination)


</p>
										<p>•Operation of Units such as Screens (Coarse / Fine Bar screens, Manual/ Mechanically operated), Sumps and Pumping Stations including Pumps, Motors and Panels (Centrifugal Horizontal / Vertical Turbine), Valves(Sluice gates, Non return, Reflux), Pipes/Specials and Pipe Joints, Grit Removal Units. Primary Sedimentation / Settling tanks, Scraping Mechanisms, Sludge withdrawal, Sludge Sumps, Sludge Pumps, Aeration tanks and Aerators, Secondary Settling Tanks, Secondary Sludge Sumps, Pumps, Sludge Thickeners, Sludge Digesters, Gas Production, Sludge handling and Drying.


</p>
										<p>•Maintenance of Pumps and Motors, Electrical Panels (Starters, Meter, (Energy, Voltage, Amperage, Power factor), Blow out Fuses, Valves, Gates, Scraping Bridge Trolley, Aerators, Reduction Gears, Open Air Weather Casings for Motors, Sprocket wheels and Chains for Mechanical Grit and Screen removing devices.


</p>
										
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar4.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>ANNUAL MAITENANCE CONTRACT</h4>
										<p>A  professional service packages which covers  the Water & waste water equipments with the most trusted people from the Industry. The preventive maintenance concept is followed to deliver value to customer.
</p>
										<p>The Annual Maintenance Contract (AMC) takes care of preventive maintenance. The AMC ensures the longevity of Components, trouble-free operations and lower downtime. Services include scheduled inspection & scheduled maintenance of the Plant.


</p>
										<p>We offer full line of repair and maintenance services for STP/ETP and other water treatment equipment. Our services include performance audit, routine plant servicing (frequency as required by customer), membrane cleaning, replacement of faulty parts etc; in addition, we also provide renovation and expansion services for your existing water and wastewater treatment plants.


</p>
										
										
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar5.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>REVAMPING AND RECOMMISSIONING OF EXISTING </h4>
										<p>We firmly believe in optimum utilization of existing plants. We offer comprehensive revamping and Recommissioning options to our customers. We study existing systems of clients and offer detailed options for meeting standards, capacity enhancement and optimum utilization of existing electro mechanical equipment.
</p>
										<p>We offer following services for Revamping and Recommissioning of Plants
</p>
										<ul>
											<li>Detailed on site study of existing systems by our qualified engineers</li>
                                            <li>In depth analysis for under performance of systems</li>
                                            <li>On Site study and trails of capacity enhancements</li>
                                            <li>Detailed reports, presentations on operational failures</li>
                                            <li>Detailed reports on proper size of equipment needed to run the plants</li>
											<li>Calibration of all electro mechanical equipments</li>
											<li>Validation for investments to revamps or Recommission the plants</li>
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar6.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SUPPLY OF ACF/PSF (MS VESSEL) </h4>
										<p>Sand filtration is frequently used and very robust method to remove suspended solids from water. The filtration medium consists of a multiple layer of sand with a variety in size and specific gravity. Sand filters can be supplied in different sizes and materials both hand operated or fully automatically.
The filter will effectively remove up to 30 – 50 micron of the suspended solids to less then 5 ppm. The filter will have to be washed with raw water for 20 to 30 minutes daily. To filter theActivated Carbon Water FilterFiber Glass Reinforced Plastic (FRP) composite vessels are 1/3 the weight of carbon steel, strengths are directly comparable to steel, no maintenance & high aesthetic appearance.

</p>
										<ul>
											<li>High performance Composite material</li>
                                            <li>Thermoplastic liner of polyester – wall thickness 3.8 mm to 7.6 mm as per vessel diameter</li>
                                            <li>100 % corrosion resistant</li>
                                            <li>Excellent bonding between inlet & PE liner</li>
                                            <li>Better curing at high temperature</li>
											<li>200 mm to 1,000 mm diameter partials below 30 – 50 micron cartridge filter is used.</li>
											
										</ul>
										<p>Applications for sand filtration :
</p>
										<ul>
											<li>Preparation of cooling water</li>
                                            <li>Treatment of waste water</li>
                                            <li>Production of drinking water</li>
                                            <li>Pre filtration for membrane systems</li>
                                            
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					

				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
