﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Solar6.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SUPPLY OF ACF/PSF (MS VESSEL) </h4>
										<p>Sand filtration is frequently used and very robust method to remove suspended solids from water. The filtration medium consists of a multiple layer of sand with a variety in size and specific gravity. Sand filters can be supplied in different sizes and materials both hand operated or fully automatically.
The filter will effectively remove up to 30 – 50 micron of the suspended solids to less then 5 ppm. The filter will have to be washed with raw water for 20 to 30 minutes daily. To filter theActivated Carbon Water FilterFiber Glass Reinforced Plastic (FRP) composite vessels are 1/3 the weight of carbon steel, strengths are directly comparable to steel, no maintenance & high aesthetic appearance.

</p>
										<ul>
											<li>High performance Composite material</li>
                                            <li>Thermoplastic liner of polyester – wall thickness 3.8 mm to 7.6 mm as per vessel diameter</li>
                                            <li>100 % corrosion resistant</li>
                                            <li>Excellent bonding between inlet & PE liner</li>
                                            <li>Better curing at high temperature</li>
											<li>200 mm to 1,000 mm diameter partials below 30 – 50 micron cartridge filter is used.</li>
											
										</ul>
										<p>Applications for sand filtration :
</p>
										<ul>
											<li>Preparation of cooling water</li>
                                            <li>Treatment of waste water</li>
                                            <li>Production of drinking water</li>
                                            <li>Pre filtration for membrane systems</li>
                                            
										</ul>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
