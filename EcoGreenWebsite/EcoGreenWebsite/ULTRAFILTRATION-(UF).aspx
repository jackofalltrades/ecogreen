﻿<%@ Page Title="ULTRAFILTATION (UF) | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="ULTRAFILTRATION-(UF).aspx.cs" Inherits="EcoGreenWebsite.ULTRAFILTRATION__UF_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WASTE WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/Wastewater6.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>ULTRAFILTATION (UF)</h4>
										<p>Ultrafiltration (UF) is a variety of membrane filtration in which forces like pressure or concentration gradients lead to a separation through a semipermeable membrane. Suspended solids and solutes of high molecular weight are retained in the so-called retentate, while water and low molecular weight solutes pass through the membrane in the permeate (filtrate). This separation process is used in industry and research for purifying and concentrating macromolecular (103 - 106 Da) solutions, especially protein solutions.

											</p>
										<p>Ultrafiltration is not fundamentally different from microfiltration. Both of these separate based on size exclusion or particle capture. It is fundamentally different from membrane gas separation, which separate based on different amounts of absorption and different rates of diffusion. Ultrafiltration membranes are defined by the molecular weight cut-off (MWCO) of the membrane used. Ultrafiltration is applied in cross-flow or dead-end mode.
</p>
										<p>Industries such as chemical and pharmaceutical manufacturing, food and beverage processing, and waste water treatment, employ ultrafiltration in order to recycle flow or add value to later products. Blood dialysis also utilizes ultrafiltration.
</p>
										<p>Ultrafiltration can be used for the removal of particulates and macromolecules from raw water to produce potable water. It has been used to either replace existing secondary (coagulation, flocculation, sedimentation) and tertiary filtration (sand filtration and chlorination) systems employed in water treatment plants or as standalone systems in isolated regions with growing populations. When treating water with high suspended solids, UF is often integrated into the process, utilising primary (screening, flotation, and filtration) and some secondary treatments as pre-treatment stages
</p>


									</div>
										<div class="clearfix"></div>
							</div>
					
                            </div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
