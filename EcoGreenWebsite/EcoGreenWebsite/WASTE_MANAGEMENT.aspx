﻿<%@ Page Title="INDUSTRIAL R.O PLANT  - Eco Green Technologies | Quality Water Mangement Plants, Solar Products sales in Coimbatore, water treatment plant in chennai, water treatment plant design in coimbatore, 
water treatment plant manufacturers in coimbatore,
water treatment plant suppliers in coimbatore,
sewage treatment plants in coimbatore,
ro water treatment plant manufacturers in coimbatore,
ro water in coimbatore,
ro water plant in coimbatore,
ro system in coimbatore,
water softener manufacturers in coimbatore,
domestic water softener in coimbatore,
domestic water treatment manufacturers in coimbatore,
industrial ro plant manufacturer in coimbatore,
industrial water treatment suppliers in coimbatore,
effluent water treatment plant manufacturers in coimbatore,
mineral water plant manufacturers in coimbatore,
ultrafiltration water treatment plant manufacturers in coimbatore,
solar power plant in coimbatore,
solar panels manufacturesin coimbatore,
water softener dealers in coimbatore,
sewage treatment plant design in coimbatore,
water treatment construction in coimbatore,
water treatment plants construction dealers in coimbatore,
wastewater treatment plant manufacturers in coimbatore,
wastewater treatment plant in coimbatore,
wastewater treatment plant suppliers in coimbatore,
solar water heating system manufacturers in coimbatore,
solar water heater suppliers in coimbatore,
solar water heater in coimbatore,
solar products manufacturers in coimbatore,
environmental consultant in coimbatore"
    Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="INDUSTRIAL-RO-PLANT.aspx.cs" Inherits="EcoGreenWebsite.INDUSTRIAL_RO_PLANT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--content-->
    <div class="content">
        <div class="advantages-w3l">
            <div class="container">
                <h3 class="w3l_head w3l_head1">WASTE MANAGEMENT SYSTEM</h3>
                <div class="advantages-grids">
                    <div class="col-md-6 advantages-grid">
                        <div class="ser-img">
                            <img class="img-responsive" src="images/Product/WasteManage1.jpg" alt=" " />
                        </div>
                    </div>
                    <div class="col-md-6 advantages-grid">
                        <div class="care">
                            <div class="left-grid">
                                <p>#</p>
                            </div>
                            <div class="right-grid">
                                <h4>ORGANIC WASTE CONVERTER</h4>
                                <p>
                                    A waste converter is a machine used for the treatment and recycling of solid and liquid refuse material. A converter is a self-contained system capable of performing the following functions: pasteurization of organic waste; sterilization of pathogenic or biohazard waste; grinding and pulverization of refuse into unrecognizable output; trash compaction; dehydration. Because of the wide variety of functions available on converters, this technology has found application in diverse waste-producing industrial segments. Hospitals, clinics, municipal waste facilities, farms, slaughterhouses, supermarkets, ports, sea vessels, and airports are the primary beneficiaries of on-site waste conversion.
                                </p>
                                <p>
                                    Converter technology is an environmentally friendly alternative to other traditional means of waste disposal that include incineration, plasma arc, and landfill dumping in that waste conversion results in a small carbon footprint, avoids polluting emissions into the atmosphere, and results in a usable end product such as biofuel, soil compost, or building material.


                                </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <!--content-->
</asp:Content>
