﻿<%@ Page Title="WATER SOFTNER | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="WATER-SOFTNER.aspx.cs" Inherits="EcoGreenWebsite.WATER_SOFTNER1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--content-->
    <div class="content">

        <div class="advantages-w3l">
            <div class="container">
                <h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
                <div class="advantages-grids">
                    <div class="col-md-6 advantages-grid">
                        <div class="ser-img">
                            <img class="img-responsive" src="images/Product/WaterManage4.jpg" alt=" " />
                        </div>
                    </div>
                    <div class="col-md-6 advantages-grid">
                        <div class="care">
                            <div class="left-grid">
                                <p>#</p>
                            </div>
                            <div class="right-grid">
                                <h4>WATER DISPENSER PLANT</h4>
                                <p>
                                    It is used to provide easy access to drinking water. Water dispensers have become a necessary part of society.
                                </p>
                                <p>
                                    Some water dispensers provide clean, filtered water from a replaceable bottle.
                                </p>
                                <p>
                                    Other water dispensers provide water straight from a municipal water line.
                                </p>
                                <p>
                                    Some dispensers perform functions like filtering water, heating water, or cooling water
                                </p>
                                <p>
                                    Water dispensers are also commonly used in residential homes that do not have ideal drinking water from the tap, or for people who simply do not like the taste of the tap water.
                                </p>
                                <p>
                                    Water dispensers are typically an environmentally friendly option, as personal bottled water leaves a lot of plastic waste.
                                </p>
                                <p>
                                    In addition, many water dispensers have the option to use filtered water, which many people enjoy more than hard water from a tap.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--content-->
</asp:Content>
