﻿<%@ Page Title="WATER SOFTNER | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="WATER-SOFTNER.aspx.cs" Inherits="EcoGreenWebsite.WATER_SOFTNER1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/Product/WaterManage3.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>WATER FILTRATION PLANT</h4>
										<p>Raw water contains undesirable sediments, colour, algae (which can produce a taste and smell) and other harmful organisms. The Water Filtration Plant (WFP) is designed to remove this undesirable matter, and produce water fit and safe for drinking
</p>
										<p>Water treatment is any process that improves the quality of water to make it more acceptable for a specific end-use. The end use may be drinking, industrial water supply, irrigation, river flow maintenance, water recreation or many other uses, including being safely returned to the environment. Water treatment removes contaminants and undesirable components, or reduces their concentration so that the water becomes fit for its desired end-use. This treatment is crucial to human health and allows humans to benefit for drinking.
</p>
										<p>Treatment for drinking water production involves the removal of contaminants from raw water to produce water that is pure enough for human consumption without any short term or long term risk of any adverse health effect. Substances that are removed during the process of drinking water treatment, Disinfection is of unquestionable importance in the supply of safe drinking-water. The destruction of microbial pathogens is essential and very commonly involves the use of reactive chemical agents such suspended solids, bacteria, algae, viruses, fungi, and minerals such as iron and manganese. 
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>
		</div>
		<!--content-->
</asp:Content>
