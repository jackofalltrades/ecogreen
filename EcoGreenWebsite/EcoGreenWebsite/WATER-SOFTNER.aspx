﻿<%@ Page Title="WATER SOFTNER | Eco Green Technologies" Language="C#" MasterPageFile="~/General.Master" AutoEventWireup="true" CodeBehind="WATER-SOFTNER.aspx.cs" Inherits="EcoGreenWebsite.WATER_SOFTNER1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--content-->
    <div class="content">

        <div class="advantages-w3l">
            <div class="container">
                <h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
                <div class="advantages-grids">
                    <div class="col-md-6 advantages-grid">
                        <div class="ser-img">
                            <img class="img-responsive" src="images/Product/WaterManage2.jpg" alt=" " />
                        </div>
                    </div>
                    <div class="col-md-6 advantages-grid">
                        <div class="care">
                            <div class="left-grid">
                                <p>#</p>
                            </div>
                            <div class="right-grid">
                                <h4>WATER SOFTNER</h4>
                                <p>
                                    Water softening is the removal of calcium, magnesium, and certain other metal cations in hard water. The resulting soft water requires less soap for the same cleaning effort, as soap is not wasted bonding with calcium ions. Soft water also extends the lifetime of plumbing by reducing or eliminating scale build-up in pipes and fittings. Water softening is usually achieved using lime softening or ion-exchange resins.
                                </p>
                                <p>
                                    The presence of certain metal ions like calcium and magnesium principally as bicarbonates, chlorides, and sulfates in water causes a variety of problems. Hard water leads to the buildup of limescale, which can foul plumbing, and promote galvanic corrosion. In industrial scale water softening plants, the effluent flow from the re-generation process can precipitate scale that can interfere with sewage systems.
                                </p>
                                <p>
                                    The slippery feeling associated with washing in soft water is caused by the weaker attraction of the soap to the water ions when the water has been stripped of its mineral content. The surface of human skin has a light charge that the soap tends to bind with, requiring more effort and a greater volume of water to remove. Hard water contains calcium or magnesium ions that form insoluble salts upon reacting with soap, leaving a coating of insoluble stearates on tub and shower surfaces, commonly called soap scum.
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="care">
                            <%--<div class="left-grid">
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
									</div>
										<div class="clearfix"></div>							
								
							</div>--%>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--content-->
</asp:Content>
