﻿<%@ Page Title="About Us  - Eco Green Technologies | Quality Water Mangement Plants, Solar Products sales in Coimbatore, water treatment plant in chennai, water treatment plant design in coimbatore, 
water treatment plant manufacturers in coimbatore,
water treatment plant suppliers in coimbatore,
sewage treatment plants in coimbatore,
ro water treatment plant manufacturers in coimbatore,
ro water in coimbatore,
ro water plant in coimbatore,
ro system in coimbatore,
water softener manufacturers in coimbatore,
domestic water softener in coimbatore,
domestic water treatment manufacturers in coimbatore,
industrial ro plant manufacturer in coimbatore,
industrial water treatment suppliers in coimbatore,
effluent water treatment plant manufacturers in coimbatore,
mineral water plant manufacturers in coimbatore,
ultrafiltration water treatment plant manufacturers in coimbatore,
solar power plant in coimbatore,
solar panels manufacturesin coimbatore,
water softener dealers in coimbatore,
sewage treatment plant design in coimbatore,
water treatment construction in coimbatore,
water treatment plants construction dealers in coimbatore,
wastewater treatment plant manufacturers in coimbatore,
wastewater treatment plant in coimbatore,
wastewater treatment plant suppliers in coimbatore,
solar water heating system manufacturers in coimbatore,
solar water heater suppliers in coimbatore,
solar water heater in coimbatore,
solar products manufacturers in coimbatore,
environmental consultant in coimbatore" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="EcoGreenWebsite.About1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- about -->
<div class="about_page">
	<div class="container">
		<h2 class="w3l_head w3l_head1">About Us</h2>
                <div class="w3ls-in-ab">
					<%--<div class="col-md-6 easy-left">
						<img src="images/img/b4.jpg" alt=" ">
					</div>--%>
					<div class="col-md-12 easy-right">
						<ul>
							<li><a href="#">ECO GREEN is one of the world’s leading companies in the water treatment and solar energy field. ECO GREEN TECH’s key competences, which are based on over 6 years of plant building experience, lie in the design, completion and operation of drinking water and wastewater treatment plants and solar projects for both the municipal and industrial sectors. ECO GREEN TECH offers sustained solutions for special customer needs through a comprehensive range of services and innovative technologies. Our plants facilitate environmentally compatible wastewater disposal and secure access to clean drinking water for an increasing number of people. This allows us to make an important contribution to environmental protection and enhanced quality of life. We would like to invite you to immerse yourself in the world of ECO GREEN TECH and thus become acquainted with a modern, internationally active company, which is rich in tradition.</a></li>
							
							<li><a href="#">Our customers have justified confidence in our blend of innovative technologies and over 10 years of plant building experience. Indeed, it is the optimum synergy between reliable quality, long-term experience and leading edge technology that has given us an outstanding, international reputation as a water technology company.</a></li>
						</ul>
						<!-- <p>Our customers have justified confidence in our blend of innovative technologies and over 10 years of plant building experience. Indeed, it is the optimum synergy between reliable quality, long-term experience and leading edge technology that has given us an outstanding, international reputation as a water technology company.</p> -->
					</div>
					<div class="clearfix"></div>
                </div>     
	</div>
</div>

<div class="about_page">
	<div class="container">
		<h2 class="w3l_head w3l_head1">Our Team</h2>
                <div class="w3ls-in-ab">					
					<div class="col-md-12 easy-right">
						 <ul>
							<li><a href="#">Managing Directors are qualified environmental engineers. They have done their post-graduation in reputed Coimbatore Institute of Technology. Their agricultural family background and personal aspiration to protect the environment have led them to take up a courier in Water management business which is a key element in keeping our environment clean.</a></li>
							
							
						</ul> 
                        <!-- <h4>Mr. Gopal and Mr. Manoj</h4>
										
						<p>Managing Directors are qualified environmental engineers. They have done their post-graduation in reputed Coimbatore Institute of Technology. Their agricultural family background and personal aspiration to protect the environment have led them to take up a courier in Water management business which is a key element in keeping our environment clean.</p> 
                        -->
					</div>
                    <%--<div class="col-md-6 easy-left">
						<img src="images/img/b1.jpg" alt=" ">
					</div>--%>
					<div class="clearfix"></div>
                </div>     
	</div>
</div>

<!-- 
<div class="team-page">
	<div class="container">
		<h3 class="w3l_head w3l_head1"> Our Team</h3>
		<div class="wthree-team">
		<div class="col-md-4 about-poleft">
            <div class="about_img"><img src="images/t2.jpg" alt="">
              <h5>Mr.Prabhu</h5>
              <div class="about_opa">
				<p>Praesent</p>
                 <div class="agile-social">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
							</ul>
				 </div>
              </div>
            </div>
        </div>
		<div class="col-md-4 about-poleft">
            <div class="about_img"><img src="images/t2.jpg" alt="">
              <h5>Mr.Suresh Kumar</h5>
              <div class="about_opa">
                <p>Lorem</p>
                 <div class="agile-social">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
							</ul>
						</div>
              </div>
            </div>
        </div>
		<div class="col-md-4 about-poleft">
            <div class="about_img"><img src="images/t2.jpg" alt="">
              <h5>Mr.Arun</h5>
              <div class="about_opa">
                <p>Ipsum</p>
                <div class="agile-social">
							<ul>
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-rss"></i></a></li>
							</ul>
						</div>
              </div>
            </div>
        </div>
		<div class="clearfix"></div>
		</div>
	</div>
</div> -->
<!-- //about -->
</asp:Content>
