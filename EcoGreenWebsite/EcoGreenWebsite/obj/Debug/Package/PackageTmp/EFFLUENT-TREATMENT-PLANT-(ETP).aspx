﻿<%@ Page Title="EFFLUENT TREATMENT PLANT (ETP) | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="EFFLUENT-TREATMENT-PLANT-(ETP).aspx.cs" Inherits="EcoGreenWebsite.EFFLUENT_TREATMENT_PLANT__ETP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/5.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>EFFLUENT TREATMENT PLANT (ETP)</h4>
										<p>ECO GREEN offers an effectual range of Effluent Treatment Plants for recycling of wastewater to reduce water demand. The comprehensive treatment solution works at various levels and involves various physical, chemical, biological and membrane processes. ECO GREEN also provides effluent treatment solutions for various types of industrial waste water.
Customized systems to suit the wide variety of effluents and to maintain efficiency are provided to industries. These systems include physic-chemical treatment, biological treatment, tertiary treatment and membrane separation process to achieve the zero-discharge standards laid by statutory authority.

									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
                            <div class="left-grid" >
										<p style="background:#FFF8F8;" ></p>
									</div>
									<div class="right-grid">
										<h4>PROCESS OVERVIEW FOR ETP</h4>
										<p>•	Screening/ Grit chamber: To remove floatable matter and sand, grit, stones in raw effluent.
•	Tertiary treatment: Sand and carbon filtration for removal of suspended solids and organic material. Ultra-filtration and Reverse Osmosis are the advanced technologies to be implemented for zero-discharge system.
•	Primary treatment: Includes chemical treatment (coagulation, flocculation, neutralization) and solid-liquid separation for removal of suspended solids.
•	Secondary Treatment: Includes biological treatment for reduction of BOD/COD and solid-liquid separation. Biological treatment can be aerobic or anaerobic depends on quality of raw effluent. Aerobic- ASP (Activated Sludge Process), MBBR/FAB (Moving Bed Bioreactor/ Fluidized Aerated Bioreactor), SAFF (Submerged Aerated Fixed Film Bioreactor), MBR (Membrane Bioreactor) Anaerobic- UASB (Up-flow anaerobic Sludge Blanket Reactor).
•	Oil & grease trap: To remove floatable oil and grease from raw effluent.
</p>

									</div>
										<div class="clearfix"></div>
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
