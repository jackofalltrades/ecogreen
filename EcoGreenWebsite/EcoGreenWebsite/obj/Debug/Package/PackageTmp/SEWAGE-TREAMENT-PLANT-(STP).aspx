﻿<%@ Page Title="SEWAGE TREAMENT PLANT (STP) | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SEWAGE-TREAMENT-PLANT-(STP).aspx.cs" Inherits="EcoGreenWebsite.SEWAGE_TREAMENT_PLANT__STP_" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/4.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SEWAGE TREAMENT PLANT (STP)</h4>
										<p>ECO GREEN offers complete solution for Sewage Water Treatment Plants and Recycle. The Sewage from the residential and commercial buildings is treated for reduction in BOD, COD & TSS in order to comply with the pollution control board standards for disposal, gardening, flushing and other non-potable purposes.
The sewage treatment plants are based on the Latest FAB (Floating Aerobic Bio media) or SAFF (Submerged Aerobic Fixed Film) treatment followed by clarification by a tube settler. Lime is dosed in for suppression of foaming tendencies. The clarified water is then further filtered in a Multimedia Sand Filter after dosing of coagulant (alum) for removal of unsettled suspended impurities followed by Sand Filter and Activated Carbon Filter for removal of organics. The filtered water from ACF is then chlorinated & stored in the treated water tank.
</p>
									</div>
                                <div class="right-grid">
								
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
