﻿<%@ Page Title="SOLAR POWER PLANT | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="SOLAR-POWER-PLANT.aspx.cs" Inherits="EcoGreenWebsite.SOLAR_POWER_PLANT1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">SOLAR PRODUCTS</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/11.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>SOLAR POWER PLANT</h4>
										<p>Solar Power Plant is the conversion of sunlight into electricity, either directly using photovoltaic or indirectly concentrated solar power. All the equipment put together to generate the electricity through solar is known as solar power plant.
•	ON-GRID SYSTEM
•	OFF-GRID SYSTEM
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
