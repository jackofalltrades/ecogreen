﻿<%@ Page Title="Contact Us" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EcoGreenWebsite.Contact1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- mail -->
	<div class="w3_map">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13251.376806128725!2d151.2012371852675!3d-33.86790584050207!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b12ae401e8b983f%3A0x5017d681632ccc0!2sSydney+NSW+2000%2C+Australia!5e0!3m2!1sen!2sin!4v1481026040523" allowfullscreen></iframe></div>
	<div class="mail">
		<div class="container">
			<h2 class="w3l_head w3l_head1">Contact Us</h2>
			<div class="agile_mail_grids">
				<div class="agile_mail_grid">
					<form action="#" method="post">
						<div class="col-md-6 agile_mail_grid_left">
							<input type="text" name="Name" placeholder="Name" required="">
							<input type="text" name="Phone" placeholder="Phone" required="">
						</div>
						<div class="col-md-6 agile_mail_grid_left">
							<input type="email" name="Email" placeholder="Email" required="">
							<input type="text" name="Subject" placeholder="Subject" required="">
						</div>
						<div class="clearfix"> </div>
						<textarea name="Message" placeholder="Message..." required=""></textarea>
						<input type="submit" value="Submit Now">
					</form>
				</div>
			</div>
			<div class="agile_mail_grid1">
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
						<h4>Contact By Email</h4>
						<p>We are happy to reply for your clarifications and feedbacks through emails. Please feel free to mail Us</p>
						<ul>
							<li>Mail1: <a href="mailto:info@example.com">ecogreentamilnadu@gmail.com</a></li>
                            <li>Mail2: <a href="mailto:info@example.com">director@ecogreentech.co.in</a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
						<h4>Contact By Phone</h4>
						<p>We are glad to hear from you 24/7. Please feel free to Contact Us</p>
						<ul>
							<li>Phone: +91 99529 99799 </li>
							<li>Phone: +91 70949 99799 </li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 agile_mail_grid1_left">
					<div class="mail_grid1_left1">
						<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
						<h4>Looking For Address</h4>
						<p>Our team members are  happy to welcome you to our Place.You are most welcome in our working hours.</p>
                        <h4  >Head Office</h4>
						<ul>
							<li > No: 10 / 43, Thotti Palayam Pirivu,</li>
							<li> Avinashi Road,</li>
                            <li>Coimbatore – 641 062</li>
						</ul>
                        <h4>Branch</h4>
                        <ul>
							<li>Branch Office: N0.9(1), Keerthana Complex, </li>
							<li>Binny Compound,Kumaran Road,</li>
                            <li>Tirupur-641 601</li>
                            <li>Ph: +91-90959 99099</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //mail -->
</asp:Content>
