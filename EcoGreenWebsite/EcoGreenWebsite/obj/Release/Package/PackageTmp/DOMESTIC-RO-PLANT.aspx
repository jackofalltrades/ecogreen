﻿<%@ Page Title="DOMESTIC R.O PLANT | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DOMESTIC-RO-PLANT.aspx.cs" Inherits="EcoGreenWebsite.DOMESTIC_RO_PLANT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/2.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>DOMESTIC R.O PLANT</h4>
										<p>Domestic Reverse Osmosis Plant is easy to use, easy to install, safe to use and provide mineral water at nominal price. With our wide range of the expertise and experience in the treating water in industrial and commercial segment, we step into the Domestic Market with the Best Unit to give the Purest Form of Water using the multiple filtrations and the state of the art technology of Reverse Osmosis, the process used for making Packaged Drinking Water & Mineral Water. Reverse Osmosis, the membrane technology removes the most of the dissolved impurities, organic impurities, toxic and heavy metal ions, bacteria and virus.


</p>
									</div>
                                <div class="right-grid">
								<h4>ADVANTAGE OVER UV</h4>
							<p> UV treatment cannot remove dissolved salts which does not alter the taste of hard water, Industrial pollutants, harmful toxic chemicals and protozoa.

</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
