﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EcoGreenWebsite.Default" %>


<!DOCTYPE html>
<html lang="en">
<head>
<title>Eco Green Technologies | Quality Water Mangement Plants, Solar Products sales in Coimbatore </title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" property="" />
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:200,300,400,700&amp;subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- banner -->
	<div class="banner">
		<div class="container">
			<div class="header">
				<div class="w3_agile_logo" style="background:rgba(255,255,255,0.3);">
                    <img src="images/img/logo.png" />
					<!-- <h1><a href="#">Eco-Green</a></h1> -->
				</div>
				<div class="w3_agileits_social_media">                    
                    <ul class="pull-right" >
						<li class="agileinfo_share">| 99529 99799</li>
                        <li class="agileinfo_share">| ecogreentamilnadu@gmail.com</li>
                    </ul>
					<ul class="pull-right">
						<li class="agileinfo_share">View On</li>
						<li><a href="#" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#" class="wthree_dribbble"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						<!--  <li><a href="#" class="wthree_behance"><i class="fa fa-behance" aria-hidden="true"></i></a></li> -->
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="ban-top">
				<div class="top_nav_left">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="navbar-collapse menu--shylock collapse" id="bs-example-navbar-collapse-1" style="height: 0px;">
						  <ul class="nav navbar-nav menu__list">
							<li class=" menu__item"><a class="menu__link" href="Default.aspx">Home <span class="sr-only">(current)</span></a></li>
							<li class=" menu__item"><a class="menu__link" href="About.aspx">About</a></li>
							
							<li class="dropdown" class=" menu__item">
								<a href="#" class="menu__link" class="dropdown-toggle" data-toggle="dropdown">Water Management System <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
                                    <li><a href="INDUSTRIAL-RO-PLANT.aspx">INDUSTRIAL R.O PLANT</a></li>
                                    <li><a href="SEWAGE-TREAMENT-PLANT-(STP).aspx">SEWAGE TREAMENT PLANT </a></li>
                                    <li><a href="EFFLUENT-TREATMENT-PLANT-(ETP).aspx">EFFLUENT TREATMENT PLANT</a></li>
                                    <li><a href="MINERAL-WATER-PLANT-(MWP).aspx">MINERAL WATER PLANT</a></li>
                                    <li><a href="ULTRAFILTRATION-(UF).aspx">ULTRAFILTRATION</a></li>
                                    <li><a href="WATER-SOFTENER.aspx">WATER SOFTENER</a></li>
								</ul>
							</li>

                              <li class="dropdown" class=" menu__item">
								<a href="#" class="menu__link" class="dropdown-toggle" data-toggle="dropdown">Solar Products <b class="caret"></b></a>
								<ul class="dropdown-menu agile_short_dropdown">
									<li><a href="SOLAR-WATER-HEATER.aspx">SOLAR WATER HEATER</a></li>
									<li><a href="SOLAR-STREET-LIGHT.aspx">SOLAR POWER PLANT</a></li>
                                    <li><a href="SOLAR-POWER-PLANT.aspx">SOLAR STREET LIGHT </a></li>

								</ul>
							</li>
                            <li class=" menu__item"><a class="menu__link" href="Gallery.aspx">Gallery</a></li>
							<li class=" menu__item"><a class="menu__link" href="Contact.aspx">Contact</a></li>                            


						  </ul>
						</div>
					  </div>
					</nav>	
				</div>
			</div>
			
			
			<section class="slider">
					<div class="flexslider">
						<ul class="slides">
							<li>
								<div class="agile_banner_info">
									<h3>Live with Nature</h3>
									<div class="agileits_w3layouts_more">
										<a href="#" data-toggle="modal" data-target="#myModal">Get More</a>
									</div>
								</div>
							</li>
							<li>
								<div class="agile_banner_info">
									<h3>Keep Surroundings Eco Friendly </h3>
									<div class="agileits_w3layouts_more">
										<a href="#" data-toggle="modal" data-target="#myModal">Get More</a>
									</div>
								</div>
							</li>
							<li>
								<div class="agile_banner_info">
									<h3>Make Use of Renewable Energy</h3>
									<div class="agileits_w3layouts_more">
										<a href="#" data-toggle="modal" data-target="#myModal">Get More</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</section>
			<!-- flexSlider -->
				<script defer src="js/jquery.flexslider.js"></script>
				<script type="text/javascript">
				    $(window).load(function () {
				        $('.flexslider').flexslider({
				            animation: "slide",
				            start: function (slider) {
				                $('body').removeClass('loading');
				            }
				        });
				    });
				</script>
			<!-- //flexSlider -->
			<div class="w3l-ban">
				<h3 style="color:white;" >An ISO 9001:2008 Certified</h3>
			</div>
		</div>
	</div>
<!-- //banner -->
<!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					An ISO 9001:2008 Certified Company
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
				</div>
				<section>
					<div class="modal-body">
						<div class="col-md-6 w3_modal_body_left">
							<img src="images/img/b1.jpg" alt=" " class="img-responsive" />
						</div>
						<div class="col-md-6 w3_modal_body_right">
							<h4>Eco Green Technologies</h4>
							<p>ECO GREEN TECH provides complete Water & Waste water Engineering Management solutions. We are specialized in designing and building Water treatment plants, Reverse Osmosis Plants, Softeners, De mineralization plants, Desalination plants. Our technical experts are best in designing and building Sewage Treatment Plant, Effluent Treatment Plant, Membrane Big reactor, Zero Liquid Discharge plants. We undertake all kinds of water treatment solutions on Turnkey basis. Furthermore, we also carry out Environmental Risk Assessment Contracts, Environmental Impact Assessment Contracts, Environmental Audits, Environmental Integration and Customization projects, Environmental Product Developments and Environmental Consultancy.
							<i>"Go Green"</i>
								</p>
						</div>
						<div class="clearfix"> </div>
					</div>
				</section>
			</div>
		</div>
	</div>
<!-- //bootstrap-pop-up -->
<!-- become -->
	<div class="become-agile">
		<div class="container">
			<div class="col-md-6 become-agile-left">
				<img src="images/img/b6.jpg" alt="" class="img-responsive">
			</div>
			<div class="col-md-6 become-agile-right">
				<h3>Eco Green Technologies</h3>
				<p>ECO GREEN TECH provides complete Water & Waste water Engineering Management solutions. We are specialized in designing and building Water treatment plants, Reverse Osmosis Plants, Softeners, De mineralization plants, Desalination plants. Our technical experts are best in designing and building Sewage Treatment Plant, Effluent Treatment Plant, Membrane Big reactor, Zero Liquid Discharge plants. We undertake all kinds of water treatment solutions on Turnkey basis. Furthermore, we also carry out Environmental Risk Assessment Contracts, Environmental Impact Assessment Contracts, Environmental Audits, Environmental Integration and Customization projects, Environmental Product Developments and Environmental Consultancy.

</p>
				<div class="agileits_w3layouts_more">
					<a href="#" data-toggle="modal" data-target="#myModal">Get More</a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //become -->
<!-- insights -->
	<!-- <div class="insights">
		<div class="container">
			<h3>consequatur voluptate</h3>
			<div class="col-md-8 insights-wthree-left">
				<div class="wthree-left_info">
					<h5>Maecenas tempus nulla</h5>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
				</div>
			</div>
			<div class="col-md-4 insights-wthree-right">
				<h4> Corporis Suscipit</h4>
				<ul class="insi">
					<li>ullam corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi</li>
					<li>laboriosam, corporis suscipit  
							nisi ut aliquid ex ea commodi</li>
					<li>suscipit corporis laboriosam, 
							nisi ut aliquid ex ea commodi</li>
					<li>corporis suscipit laboriosam, 
							nisi ut aliquid ex ea commodi</li>
				</ul>
			</div>
				<div class="clearfix"> </div>
			<div class="insights-bottom">
				<div class="col-md-4 insights-bottom-left">
					<img src="images/g2.jpg" alt="" class="img-responsive">
					<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
				</div>
				<div class="col-md-4 insights-bottom-middle">
					<img src="images/g3.jpg" alt="" class="img-responsive">
					<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
				</div>
				<div class="col-md-4 insights-bottom-right">
					<img src="images/g4.jpg" alt="" class="img-responsive">
					<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here</p>
				</div>
					<div class="clearfix"> </div>
			</div>
		</div>
	</div>-->
<!-- //insights -->
<!-- agile -->
	<div class="agile">
		<div class="container">
			<div class="agile_right">
				<div class="col-md-4 list-left text-center wow fadeInLeft animated animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
					<span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
					<h4>Quality Products</h4>
					<p>Our Products are having world class ranking in international markets. Its named for standard and quality. </p>
				</div>
				<div class="col-md-4 list-left text-center wow fadeInUp animated animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
					<span class="glyphicon glyphicon-oil" aria-hidden="true"></span>
					<h4>Awarded Support</h4>
					<p>We have with us highly Qualified and Trained Engineers, and other professional in the field of marketing who are committed to Excellence in Quality and Customer satisfaction.</p>
				</div>
				<div class="col-md-4 list-left text-center wow fadeInRight animated animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight;">
					<span class="glyphicon glyphicon-compressed" aria-hidden="true"></span>
					<h4>Easy Maintenance</h4>
					<p>We offer sustainable solutions that can serve as the economic basis for a region and provide enhanced quality of life for the local population.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
<!-- //agile -->
<!-- features -->
	<div class="features">
		<div class="container">
			<h3>Products</h3>
				<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					<ul id="myTab" class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true">Water Management System</a></li>
						<li role="presentation"><a href="#Feature1" role="tab" id="Feature1-tab" data-toggle="tab" aria-controls="Feature1">Solar Products</a></li>
						<!-- <li role="presentation"><a href="#Feature2" role="tab" id="Feature2-tab" data-toggle="tab" aria-controls="Feature2">Feature-3</a></li>
						<li role="presentation"><a href="#Feature3" role="tab" id="Feature3-tab" data-toggle="tab" aria-controls="Feature3">Feature-4</a></li> -->
					</ul>
					<div id="myTabContent" class="tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="home" aria-labelledby="home-tab">
							<div class="w3agile_tabs">
								<div class="col-md-7 w3agile_tab_left">
									<h4>Water Management System</h4>
									<p>Water treatment is any process that improves the quality of water to make it more acceptable for a specific end-use. The end use may be drinking, industrial water supply, irrigation, river flow maintenance, water recreation or many other uses, including being safely returned to the environment. Water treatment removes contaminants and undesirable components, or reduces their concentration so that the water becomes fit for its desired end-use.</p>
									<ul>
										
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>INDUSTRIAL R.O </li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>SEWAGE TREAMENT </li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>EFFLUENT TREATMENT </li>
                                        <li><i class="fa fa-arrow-right" aria-hidden="true"></i>MINERAL WATER </li>
                                         <li><i class="fa fa-arrow-right" aria-hidden="true"></i>ULTRAFILTRATION </li>
                                        <li><i class="fa fa-arrow-right" aria-hidden="true"></i>WATER SOFTENER </li>
									</ul>
								</div>
								<div class="col-md-5 w3agile_tab_right w3agile_tab_right1">
									<img src="images/img/b2.jpg" alt=" " class="img-responsive" />
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="Feature1" aria-labelledby="Feature1-tab">
							<div class="w3agile_tabs">
								<div class="col-md-5 w3agile_tab_right w3agile_tab_right2">
									<img src="images/img/b1.jpg" alt=" " class="img-responsive" />
								</div>
								<div class="col-md-7 w3agile_tab_left">
									<h4>Solar Products</h4>
									<p>Solar power is the key to a clean energy future. Every day, the sun gives off far more energy than we need to power everything on earth. Solar panels produce electricity by transforming the continuous flow of energy from the sun to electricity.No harmful emissions are released into the air when electricity is produced by solar panels.</p>
									<ul>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>SOLAR WATER HEATER</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>SOLAR POWER PLANT </li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>SOLAR STREET LIGHT</li>
										<!-- <li><i class="fa fa-arrow-right" aria-hidden="true"></i>Phasellus libero tellus sem</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Gravida eget auctor eros</li> -->
									</ul>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<!-- <div role="tabpanel" class="tab-pane fade" id="Feature2" aria-labelledby="Feature2-tab">
							<div class="w3agile_tabs">
								<div class="col-md-7 w3agile_tab_left">
									<h4>faucibus consectetur neque sollicitudin</h4>
									<p>Nunc faucibus lorem a arcu gravida, eget auctor eros 
										ultrices. Vestibulum non erat ut odio euismod accumsan. 
										Phasellus libero tellus, pulvinar vitae sem sit amet, 
										faucibus consectetur neque.</p>
									<ul>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Faucibus consectetur neque</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Erat odio euismod accumsan</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Sapien nec interdum euismod</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Phasellus libero tellus sem</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Gravida eget auctor eros</li>
									</ul>
								</div>
								<div class="col-md-5 w3agile_tab_right w3agile_tab_right1">
									<img src="images/g3.jpg" alt=" " class="img-responsive" />
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="Feature3" aria-labelledby="Feature3-tab">
							<div class="w3agile_tabs">
								<div class="col-md-5 w3agile_tab_right w3agile_tab_right2">
									<img src="images/g4.jpg" alt=" " class="img-responsive" />
								</div>
								<div class="col-md-7 w3agile_tab_left">
									<h4>gravida eget auctor eros libero tellus</h4>
									<p>Nunc faucibus lorem a arcu gravida, eget auctor eros 
										ultrices. Vestibulum non erat ut odio euismod accumsan. 
										Phasellus libero tellus, pulvinar vitae sem sit amet, 
										faucibus consectetur neque.</p>
									<ul>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Faucibus consectetur neque</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Erat odio euismod accumsan</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Sapien nec interdum euismod</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Phasellus libero tellus sem</li>
										<li><i class="fa fa-arrow-right" aria-hidden="true"></i>Gravida eget auctor eros</li>
									</ul>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div> -->
					</div>
				</div>
		</div>
	</div>
<!-- //features -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="col-md-4 agileinfo_footer_grid">
				<h3>Ping Us</h3>
				<p>We support the eco friendly surrounding. We are happy to hear from our valuable customers.</p>
				<form action="#" method="post">
					<input type="email" name="Email" placeholder="Email" required="">
					<input type="submit" value="Send">
				</form>
			</div>
			<div class="col-md-4 agileinfo_footer_grid">
				<h3>ABOUT US</h3>
				<p>Eco-Green Technologies regard our endeavors aimed at the conservation and ecological utilization of the world’s most valuable resource as both a daily challenge and a social responsibility. Moreover, we believe that these activities constitute a major investment in a future life that will be worth living.</p>
			</div>
			<div class="col-md-4 agileinfo_footer_grid">
				<h3>CONTACT</h3>
				<p>No: 10 / 43, Thotti Palayam Pirivu, <br /> 
                    Avinashi Road,  Coimbatore – 641 062 <br />
                    +91 99529 99799 / +91 70949 99799 <br />
                    ecogreentamilnadu@gmail.com, director@ecogreentech.co.in </p>

			</div>
		</div>
	</div>
<!-- //footer -->
<!-- copy-right -->
	<div class="w3agile_copy_right">
		<div class="container">
			<p>© 2018 Eco Green Technologies. All Rights Reserved | Design by <a href="http://drizzlingapp.com/" target="_blank">Drizzling App</a></p>
		</div>
	</div>
<!-- //copy-right -->
<!-- for bootstrap working -->
	<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
</body>
</html>