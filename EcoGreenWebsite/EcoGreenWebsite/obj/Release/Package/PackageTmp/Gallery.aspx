﻿<%@ Page Title="Gallery" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="EcoGreenWebsite.Gallery1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--<title>Gallery - Eco Green Technologies </title>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!-- Gallery -->
<div class="agileinfo-gal">
		<div class="container">
		<h2 class="w3l_head w3l_head1">Gallery</h2>
			<div class="agileits_portfolio_grids">
				<div class="col-md-3 agileits_portfolio_grid">
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g1.jpg">
							<img src="images/1.jpeg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>R.O (REVERSE OSMOSIS) PLANT</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g2.jpg">
							<img src="images/2.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>DOMESTIC R.O PLANT</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g3.jpg">
							<img src="images/3.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>INDUSTRIAL R.O PLANT</p> 
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-3 agileits_portfolio_grid">
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g4.jpg">
							<img src="images/4.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SEWAGE TREAMENT PLANT (STP)</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g5.jpg">
							<img src="images/5.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>EFFLUENT TREATMENT PLANT (ETP)</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g6.jpg">
							<img src="images/6.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>MINERAL WATER PLANT (MWP)</p> 
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-3 agileits_portfolio_grid">
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g7.jpg">
							<img src="images/7.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>ULTRAFILTRATION (UF)</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g8.jpg">
							<img src="images/8.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>WATER SOFTENER</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g9.jpg">
							<img src="images/9.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR WATER HEATER</p> 
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-3 agileits_portfolio_grid">
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g10.jpg">
							<img src="images/10.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR STREET LIGHT</p> 
							</div>
						</a>
					</div>
					<div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/11.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/12.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/13.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/14.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/15.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/16.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
                    <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g11.jpg">
							<img src="images/17.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>ECO GREEN</h4>
								<p>SOLAR POWER PLANT</p> 
							</div>
						</a>
					</div>
					<!-- <div class="agileinfo_portfolio_grid hovereffect">
						<a class="cm-overlay" href="images/g12.jpg">
							<img src="images/g12.jpg" alt=" " class="img-responsive">
							<div class="overlay">
								<h4>Automated</h4>
								<p>Nulla dui diam, gravida nec eros ut, congue.</p> 
							</div>
						</a>
					</div> -->
				</div>
				<div class="clearfix"> </div>
			</div>
				<script src="js/jquery.tools.min.js"></script>
				<script src="js/jquery.mobile.custom.min.js"></script>
				<script src="js/jquery.cm-overlay.js"></script>
				<script>
				    $(document).ready(function () {
				        $('.cm-overlay').cmOverlay();
				    });
				</script>
		</div>
	</div>
<!-- //Gallery -->
</asp:Content>
