﻿<%@ Page Title="INDUSTRIAL R.O PLANT | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="INDUSTRIAL-RO-PLANT.aspx.cs" Inherits="EcoGreenWebsite.INDUSTRIAL_RO_PLANT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/3.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>INDUSTRIAL R.O PLANT</h4>
										<p>ECO GREEN TECH offers the State-of-the-Art Packaged Reverse Osmosis Systems complete with Pre & Post Treatment equipment offering Very Pure Water from Brackish or Saline water. Water Produced using Reverse Osmosis Technology is of very Low TDS & free from Particulate, Organic and Colloidal matter. ECO GREEN TECH carries complete range Reverse Osmosis Plant. Units ranging from 100 to 10000 LPH and more.Our Standard features of reverse osmosis plant includes Pre Micron Filter, High Pressure Pump, Stainless Steel Pressure tubes with Membranes, Electrical Control Panel with TDS meter and Wet Panel with Flow Indicators and Pressure Gauges, pressure switch and electrically driven solenoid valve, Pressure Regulator valve, Stainless steel 304 frame (Skid) etc. These systems are used for water having TDS up to 2000 ppm.


</p>
									</div>
                                <div class="right-grid">
								
							<p> 

</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
