﻿<%@ Page Title="R.O (REVERSE OSMOSIS) PLANT | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="RO-PLANT.aspx.cs" Inherits="EcoGreenWebsite.RO_PLANT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/1.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>R.O (REVERSE OSMOSIS) PLANT</h4>
										<p>Reverse osmosis is a well-tried and tested technology for the production of desalinated water for use as both drinking and industrial process water.ECO GREEN TECH supplies RO plants in differing sizes for various types of raw water such as sea and brackish water and industrial effluent.
Major attention is paid to economic factors such as chemical consumption, energy recovery, membrane durability and process automation.
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
