﻿<%@ Page Title="ULTRAFILTATION (UF) | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ULTRAFILTRATION-(UF).aspx.cs" Inherits="EcoGreenWebsite.ULTRAFILTRATION__UF_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/7.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>ULTRAFILTATION (UF)</h4>
										<p>We are leading names in the field of offering competitive solutions for Ultra filtration systems.
These are systems based on separation process using membranes having pore sizes in range of 0.1 to 0.001 micron and provide assistance in removing colloidal materials, high molecular-weight substances as well as organic & inorganic polymeric molecules.



•	It is asymmetric semi-permeable membrane made using high molecular material.
•	The hollow fiber tubes are covered by micro-pores that allow solutions flowing in/out the membranes under influence of pressure.
•	UF membranes have pore sizes from 0.1 to 0.005 µm for different applications.
•	Normally, this process is utilized in removing-
o	High molecular-weight substances
o	Colloidal materials
o	Bacteria
o	Organic/inorganic polymeric molecules

</p>
                                        <p>•	It is asymmetric semi-permeable membrane made using high molecular material.
•	The hollow fiber tubes are covered by micro-pores that allow solutions flowing in/out the membranes under influence of pressure.
•	UF membranes have pore sizes from 0.1 to 0.005 µm for different applications.
•	Normally, this process is utilized in removing-
o	High molecular-weight substances
o	Colloidal materials
o	Bacteria
o	Organic/inorganic polymeric molecules

</p>
									</div>
										<div class="clearfix"></div>
							</div>
					
                            </div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
