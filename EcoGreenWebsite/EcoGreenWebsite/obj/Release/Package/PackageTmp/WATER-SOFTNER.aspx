﻿<%@ Page Title="WATER SOFTNER | Eco Green Technologies" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WATER-SOFTNER.aspx.cs" Inherits="EcoGreenWebsite.WATER_SOFTNER1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<!--content-->
		<div class="content">
			
			<div class="advantages-w3l">
				<div class="container">
				<h3 class="w3l_head w3l_head1">WATER MANAGEMENT SYSTEM</h3>
					<div class="advantages-grids">
						<div class="col-md-6 advantages-grid">
							<div class="ser-img">
								<img class="img-responsive" src="images/8.jpg" alt=" "/>
							</div>
						</div>
						<div class="col-md-6 advantages-grid">
							<div class="care">
									<div class="left-grid">
										<p>#</p>
									</div>
									<div class="right-grid">
										<h4>WATER SOFTNER</h4>
										<p>ECO GREEN TECH offers Water Softening System in various sizes ranging from 200 liters/hr. up to 500 m3/hr. and above. These units are available in Stainless Steel option. Units are manufactures using stainless steel pressure vessel, internal distribution and collection system, multiport or individual valves, piping, venture for salt injection, salt tank, test kit etc.
</p>
									</div>
										<div class="clearfix"></div>
							</div>
							<div class="care">
								<h4>Special Features:</h4>
										<p>Pre-Assembled, Factory Tested Systems, Compact, easy to Install, Operate and Maintain Stainless Steel vessels, SS Piping & Valves and SS 304 Skid for mounting units System requires only Water, Electrical and Drain supply and can be installed in a day. Multi-Port Control valve for easy operation of Filtration and Electrical Controls with all interlocks Well Packed - Resin media to provide good Filtered Water Quality. Various models available are as follows: These Softeners are available from 200 Mm Dia up to 2000 mm Dia in Stainless steel construction, while for higher Flow Rates, same is available on custom design basis. Water produced using these Softeners is used widely in Industry and Commercial Sector.</p>
								
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			
		</div>		
		<!--content-->
</asp:Content>
